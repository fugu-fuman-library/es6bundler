<?php

namespace Fuman\Es6Builder;

class Config {
    public function __construct(
        /**
         * Path of the webroot (or common ancestor directory of all files).
         */
        public string $rootPath
        /**
         * Directory/Path names/patterns of the bundles
         * @var String[]
         */
        , public array $bundles
        , public array $ignoreFileNamePatterns = ['test']
        , public string $dynamicModuleImportFunctionName = 'fuImport'
    ) { }
}
