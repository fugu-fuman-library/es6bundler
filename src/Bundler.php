<?php
namespace Fuman\Es6Builder;
use Fuman\Es6Builder\Transformer\Basic;
use Fuman\Es6Builder\Transformer\Package;

/**
 * ES6 Javascript Bundler/Transformer.
 * This Bundler packs configured directories to a single file or transforms the import of single files according to the packed directories.
 *
 * @package JsBundler
 */
class Bundler {
    public function __construct(protected Config $config){}

    /**
     * @param string $path
     * @return string
     */
    public function transformFile(string $path):string {
        $path = str_replace('\\', '/', $path);
        $pathInfo = PathInfo::getInstanceFromPath($path, $this->config);

        if($pathInfo->isPackage) {
            return (new Package($pathInfo))->transform();

        }

        return (new Basic($pathInfo))->transform();
    }
}