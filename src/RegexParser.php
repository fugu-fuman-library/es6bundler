<?php

namespace Fuman\Es6Builder;

use Fuman\Es6Builder\Statement\DynamicImport;
use Fuman\Es6Builder\Statement\Export;
use Fuman\Es6Builder\Statement\Import;

class RegexParser implements Parser {
    const REGEX = '/(?|(?:^|\s+)'
    . '(?|(import)\s+(?:(\w*?)(?:\s*,\s*)?(\*\s+as\s+\w+)?(?:{([^}]+)})?\s+from\s+)?([\'"])(.+?)\5([^\n;]*;?)'
    . '|(export)\s+(default\s+)?(?|(?|({)([^};]+)}|(\*))((\s+from\s+)([\'"])(.+?)\7)?|((?:async\s+)?\w+\s+)?([\w_]+))([^\n;]*;?))'
    . '|[^\w_](import\s*\()[\'"]'
    . '|(\/\*).+?\*\/'
    . '|(\/\/)[^\n]+'
    . ')/ius';

    public function parse(SourceInfo $sourceInfo): void {
        preg_match_all(static::REGEX, $sourceInfo->source, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);

        foreach($matches as $match) {
            switch($match[1][0]) {
                case 'import':
                    $import = new Import($match[1][1]);
                    $import->defaultSource = $match[2][0];
                    $import->nameSpaceSource = $match[3][0];
                    $import->identifierSource = $match[4][0];
                    $import->pathSource = trim($match[6][0]);//Use of regex dot
                    $import->endIndex = $this->catchLineEnding($sourceInfo, $match[7][1] + strlen($match[7][0]));
                    
                    $sourceInfo->addImport($import);
                    break;
                case 'export':
                    $export = new Export($match[1][1]);
                    $export->isDefault = $match[2][0] !== '';
                    //A default expression could have an empty "type" at index 3 (because it is moved to index 4)
                    $export->endIndex = $export->isDefault && $match[3][1] === -1 ? $match[4][1] : $match[3][1];

                    if($match[3][0] === '{') {
                        $export->exportSource = trim($match[4][0]);//Can include whitespace
                        $export->pathSource = trim($match[8][0]);//Use of regex dot
                        $export->endIndex = $this->catchLineEnding($sourceInfo, $match[9][1] + strlen($match[9][0]));
                    }
                    else {
                        $identifierSource = $match[3][0] === '*' ? '*' : trim($match[4][0]);//Can include whitespace
                        
                        //Check that the identifier source is not a valid javascript type
                        if(!$export->isDefault || !in_array($identifierSource, ['function', 'class', 'const', 'let', 'extends'])){
                            $export->identifierSource = $identifierSource;
                        }

                        //handle some stupid formatting fails from rollup etc., which moves exports to a separate statement;
                        //But ignore this format if binding/type is set like const/function/class etc. in $match[3][0]
                        if(empty($match[3][0]) && strpos($match[9][0], ';') !== false) {
                            $export->endIndex = $match[9][1] + strlen($match[9][0]);
                        }
                    }

                    $sourceInfo->exports[] = $export;
                    break;
                case '/*':
                case '//':
                    //ignore comments
//                    var_dump($match[0][0]);
                    break;
                default:
                    if(str_starts_with($match[1][0], 'import')) {
                        $dynamicImport = new DynamicImport($match[1][1]);
                        $dynamicImport->endIndex = $dynamicImport->startIndex + strlen($match[1][0]);
                        $sourceInfo->dynamicImports[] = $dynamicImport;
                    }
                    else {
                        throw new \UnexpectedValueException($match[1][0]);
                    }
            }
        }
    }
    
    public function catchLineEnding(SourceInfo $sourceInfo, $pos):int {
        return substr($sourceInfo->source, $pos, 1) === "\n" ? $pos + 1 : $pos;
    }
}