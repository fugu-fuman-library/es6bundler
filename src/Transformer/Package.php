<?php
namespace Fuman\Es6Builder\Transformer;

use Fuman\Es6Builder\PackageInfo;
use Fuman\Es6Builder\PathInfo;
use Fuman\Es6Builder\SourceInfo;
use Fuman\Es6Builder\Util;

class Package extends Basic {
    protected PackageInfo $packageInfo;

    /** @var SourceInfo[] */
    protected array $nextSources = [];

    /** @var string[][] */
    protected array $dependencyMap = [];

    /** @var SourceInfo[] */
    protected array $sourceInfoMap = [];

    public function __construct(PathInfo $pathInfo) {
        parent::__construct($pathInfo);
        $this->packageInfo = new PackageInfo();
    }

    public function isValidFileName($fileName){
        foreach($this->pathInfo->config->ignoreFileNamePatterns as $pattern){
            if(str_contains($fileName, $pattern)){
                return false;
            }
        }

        return true;
    }
    
    public function transform(): string {
        $dir = new \RecursiveDirectoryIterator(dirname($this->pathInfo->packagePath));
        $files = new \RecursiveIteratorIterator($dir);

        /** @var \SplFileInfo $file */
        foreach($files as $file) {
            if($file->getExtension() === 'js' && $this->isValidFileName($file->getFilename())) {
                $sourceInfo = $this->createSourceInfo(PathInfo::getInstanceFromPath(Util::normalizePath($file->getRealPath()), $this->pathInfo->config));

                if(empty($sourceInfo->dependencies)){
                    $this->nextSources[] = $sourceInfo;
                }
                else {
                    $this->sourceInfoMap[$sourceInfo->pathInfo->path] = $sourceInfo;
                    foreach($sourceInfo->dependencies as $dependency){
                        $this->dependencyMap[$dependency->importPathInfo->path][] = $dependency->sourcePathInfo->path;
                    }
                }
            }
        }
        
        foreach($this->dependencyMap as $path=>&$importPaths){
            foreach($importPaths as $index=>$importPath){
                if(isset($this->dependencyMap[$importPath])){
                    $importPaths2 = &$this->dependencyMap[$importPath];
                    $index2 = array_search($path, $importPaths2);
                    
                    if($index2 !== false){
                        array_splice($importPaths2, $index2, 1);
                        array_splice($importPaths, $index, 1);
                        $this->sourceInfoMap[$path]->mergeSourceInfo($this->sourceInfoMap[$importPath]);
                        $this->sourceInfoMap[$importPath] = $this->sourceInfoMap[$path];
//                        var_dump('------', $path, $importPath);
                        break;
                    }
                }
            }
        }
        
        return $this->generateImportStatements() . $this->combineSource();
    }
    
    public function createSourceInfo(PathInfo $pathInfo){
        $sourceInfo = new SourceInfo($pathInfo);
        $this->parser->parse($sourceInfo);
        $importedIdentifiers = [];

        foreach($sourceInfo->imports as $import) {
            $ic = $import->identifierCollection;
            array_push($importedIdentifiers, ...array_values($ic->identifiers));

            if($import->samePackage) {
                $sourceInfo->addDependency($ic);
                continue;
            }

            $this->importIndex[$ic->importPathInfo->key][] = $ic;
        }
        
        $filteredExports = [];
        foreach($sourceInfo->exports as $export) {
            if(empty($export->pathSource)){
                //Filter out proxy or gateway exports
                if(!($export->isDefault || empty($export->exportSource))){
                    $ic = $export->createIdentifierCollection($pathInfo);
                    if(count(array_intersect($importedIdentifiers, array_keys($ic->identifiers))) === count($ic->identifiers)){
                        $sourceInfo->replacements[] = [$export->startIndex, $export->endIndex, ''];
                        continue;
                    }
                }
                
                $filteredExports[] = $export;
            }
            //If exports is importing stuff, add mapping information and add to dependency if it belongs to the same package
            else {
                $sourceInfo->replacements[] = [$export->startIndex, $export->endIndex, ''];

                $ic = $export->createIdentifierCollection($pathInfo);
                $this->packageInfo->addIdentifierAliases($ic);

                if($pathInfo->key === $ic->importPathInfo->key){
                    $sourceInfo->addDependency($ic);
                }
            }
        }
        $sourceInfo->exports = $filteredExports;
        
        return $sourceInfo;
    }
    
    public function applyTransformation(SourceInfo $firstSourceInfo){
        $functionArguments = $functionParameters = [];
        $exportStatements = $returnStatements = [];
        $source = '';

        foreach($firstSourceInfo->getAllSourceInfos() as $sourceInfo){
            $pathInfo = $sourceInfo->pathInfo;
            
            foreach($sourceInfo->imports as $import) {
                $sourceInfo->replacements[] = [$import->startIndex, $import->endIndex, ''];

                if($import->samePackage){
                    $identifierCollections = $import->identifierCollection->updateAliasedIdentifiers($this->packageInfo);

                    if(empty($identifierCollections)){
                        $import->identifierCollection->injectFunctionParametersAndArguments($functionParameters, $functionArguments);
                    }
                    else {
                        foreach($identifierCollections as $identifierCollection){
                            $identifierCollection->injectFunctionParametersAndArguments($functionParameters, $functionArguments);
                        }
                    }
                }
                else {
                    $import->identifierCollection->injectFunctionParametersAndArguments($functionParameters, $functionArguments);
                }
            }
            
            foreach($sourceInfo->exports as $export){
                $sourceInfo->replacements[] = [$export->startIndex, $export->endIndex, $export->createReplacement($pathInfo->identifierPrefix)];
                $exportStatements[] = $export->createExportStatementPart($pathInfo);
                $returnStatements[] = $export->createReturnStatementPart();
            }

            $sourceInfo->applyReplacements();
            $source .= $sourceInfo->source;
        }
        
        if(!empty($firstSourceInfo->mergedSourceInfos)){
            $exportStatements = array_unique($exportStatements);
            $returnStatements = array_unique($returnStatements);
            $functionParameters = array_unique($functionParameters);
            $functionArguments = array_unique($functionArguments);
        }

        if(empty($exportStatements)){
            $firstSourceInfo->source = '';
        }
        else {
            if(empty($returnStatements)){
                $returnStatementString = $resultStatementString = '';
            }
            else {
                $returnStatementString = "\n\nreturn [" . implode(',', $returnStatements) . '];';
                $resultStatementString = "const [" . implode(', ', $exportStatements) . '] = ';
            }

            $source = $resultStatementString . '(function('.implode(',', $functionParameters)."){\n\n" . $source . $returnStatementString . "\n\n})(".implode(',', $functionArguments).');';
            $firstSourceInfo->source = $source . "\nexport {" . implode(', ', $exportStatements) . "};\n";
        }
    }
    
    public function combineSource(): string {
        $source = '';
        while(!empty($this->nextSources)){
            $sourceInfo = array_shift($this->nextSources);
            $path = $sourceInfo->pathInfo->path;
            
            if(/*substr($path, -8) !== 'index.js' && */(isset($this->dependencyMap[$path]) || !$sourceInfo->hasOnlyProxyExports())){
                $this->applyTransformation($sourceInfo);
                $source .= $sourceInfo->source;
            }
            
            foreach($sourceInfo->getAllSourceInfos() as $sourceInfo){
                $path = $sourceInfo->pathInfo->path;
                if(isset($this->dependencyMap[$path])){
                    foreach($this->dependencyMap[$path] as $dependencySourcePath){
                        $depSourceInfo = $this->sourceInfoMap[$dependencySourcePath];
                        if($depSourceInfo->setDependencyResolved($path)->areAllDependenciesResolved()){
                            if(!in_array($depSourceInfo, $this->nextSources)){
                                $this->nextSources[] = $depSourceInfo;    
                            }
                        }
                    }

                    unset($this->dependencyMap[$path]);
                }
            }
        }

        if(!empty($this->dependencyMap)){
            foreach($this->dependencyMap as $key=>$deps){
                var_dump($key, 'is required by:');
                foreach($deps as $dep)
                    var_dump("-\t" . $dep);
            }

            throw new \Exception('cyclic dependencies detected');
        }

        return $source;
    }
    
    protected function generateIdentifierPartForImportStatement(bool $isPackage, string $prefix, string $identifier, string $alias): string {
        return $isPackage ? $prefix . $identifier : ($identifier === 'defaultExport' ? 'default' : $identifier) . ' as ' . $prefix . $identifier;
    }
}