<?php
namespace Fuman\Es6Builder\Transformer;

use Fuman\Es6Builder\Descriptor\IdentifierCollection;
use Fuman\Es6Builder\Parser;
use Fuman\Es6Builder\PathInfo;
use Fuman\Es6Builder\RegexParser;
use Fuman\Es6Builder\SourceInfo;
use Fuman\Es6Builder\Util;

class Basic {
    protected Parser $parser;
    
    /** @var IdentifierCollection[][] */
    protected array $importIndex = [];
    
    protected PathInfo $pathInfo;
    
    public function __construct(PathInfo $pathInfo) {
        $this->parser = new RegexParser();
        $this->pathInfo = $pathInfo;
    }
    
    public function transform(): string {
        $sourceInfo = new SourceInfo($this->pathInfo);
        $this->parser->parse($sourceInfo);

        foreach($sourceInfo->imports as $import) {
            $import->init($this->pathInfo);

            //replace imports only if we import a package, otherwise don't touch running code!!! ;)
            if($import->pathInfo->isPackage){
                $sourceInfo->replacements[] = [$import->startIndex, $import->endIndex, ''];
                $this->importIndex[$import->pathInfo->key][] = $import->identifierCollection;
            }
        }
        
        $sourceInfo->applyReplacements();
        
        return $this->generateImportStatements() . $sourceInfo->source;
    }
    
    public function generateImportStatements(): string {
        $source = [];
        
        foreach($this->importIndex as $identifierCollections){
            $identifiers = [];

            foreach($identifierCollections as $identifierCollection){
                $pathInfo = $identifierCollection->importPathInfo;
                $isPackage = $identifierCollection->importPathInfo->isPackage;
                $prefix = $identifierCollection->importPathInfo->identifierPrefix;

                foreach($identifierCollection->identifiers as $identifier=>$alias){
                    $identifiers[] = $this->generateIdentifierPartForImportStatement($isPackage, $prefix, $identifier, $alias);
                }
            }

            /** @noinspection PhpUndefinedVariableInspection (we know that $pathInfo has been created, as their must not exists any empty list entry) */
            $relativePath = Util::createRelativePath($this->pathInfo->path, $pathInfo->packagePath);
            $identifiers = implode(', ', array_unique($identifiers));
            
            $source[] = "import {{$identifiers}} from '{$relativePath}';\n";
        }
        
        return implode("\n", $source);
    }

    protected function generateIdentifierPartForImportStatement(bool $isPackage, string $prefix, string $identifier, string $alias): string {
        //We only handle imports from packages, the other are left untouched in the basic transformer!
        return $prefix . $identifier . ' as ' . $alias;
    }
}