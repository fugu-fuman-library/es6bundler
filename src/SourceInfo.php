<?php
namespace Fuman\Es6Builder;

use Fuman\Es6Builder\Descriptor\IdentifierCollection;
use Fuman\Es6Builder\Statement\DynamicImport;
use Fuman\Es6Builder\Statement\Export;
use Fuman\Es6Builder\Statement\Import;


/**
 * Holds the information about imports and exports of a (virtual) file.
 */
class SourceInfo {
    public PathInfo $pathInfo;
    
    /** @var IdentifierCollection[] */
    public array $dependencies;
    
    public string $source;
    
    public array $replacements;
    
    /** @var SourceInfo[] */
    public array $mergedSourceInfos;

    /** @var Import[]  */
    public array $imports;
    
    /** @var DynamicImport[]  */
    public array $dynamicImports;
    
    /** @var Export[] */
    public array $exports;
    
    /**
     * SourceInfo constructor.
     *
     * @param PathInfo $pathInfo
     */
    public function __construct(PathInfo $pathInfo) {
        $this->pathInfo = $pathInfo;
        $this->source = file_get_contents($pathInfo->path);
        $this->dependencies = [];
        $this->replacements = [];
        $this->imports = [];
        $this->exports = [];
        $this->dynamicImports = [];
    }
    
    public function addDependency(IdentifierCollection $identifierCollection){
        $this->dependencies[$identifierCollection->importPathInfo->path] = $identifierCollection;
    }
    
    public function setDependencyResolved($path): SourceInfo {
        unset($this->dependencies[$path]);
        return $this;
    }
    
    public function areAllDependenciesResolved(): bool {
        return empty($this->dependencies);
    }
    
    public function applyReplacements(): SourceInfo {
        foreach($this->dynamicImports as $dynamicImport) {
            $this->replacements[] = [$dynamicImport->startIndex, $dynamicImport->endIndex, $dynamicImport->createReplacement($this->pathInfo)];
        }

        usort($this->replacements, function($a, $b) { return $b[0] - $a[0]; });
        foreach($this->replacements as $replacement) {
            $this->source = substr($this->source, 0, $replacement[0]) . $replacement[2] . substr($this->source, $replacement[1]);
        }
        
        return $this;
    }
    
    public function mergeSourceInfo(SourceInfo $sourceInfo){
        $this->mergedSourceInfos[] = $sourceInfo;
        
        $this->setDependencyResolved($sourceInfo->pathInfo->path);
        $sourceInfo->setDependencyResolved($this->pathInfo->path);
        
        
        $this->removeImport($sourceInfo->pathInfo->path);
        $sourceInfo->removeImport($this->pathInfo->path);
        
        foreach($sourceInfo->dependencies as $path=>$dependency){
            $this->dependencies[$path] = $dependency;
        }
    }
    
    public function removeImport($path){
        $import = $this->imports[$path];
        $this->replacements[] = [$import->startIndex, $import->endIndex, ''];
        unset($this->imports[$path]);
    }

    /**
     * @return SourceInfo[]
     */
    public function getAllSourceInfos(): array {
        return [$this, ...(empty($this->mergedSourceInfos) ? [] : $this->mergedSourceInfos)];
    }
    
    public function hasOnlyProxyExports(): bool {
        foreach($this->exports as $export){
            if(empty($export->pathSource)){
                return false; 
            }
        }
        return true;
    }
    
    public function addImport(Import $import){
        $import->init($this->pathInfo);
        $this->imports[$import->pathInfo->path] = $import;
    }
}