<?php
namespace Fuman\Es6Builder;


use Fuman\Es6Builder\Descriptor\IdentifierCollection;

class PackageInfo {
    public $identifierAliasList;
    
    public function addIdentifierAliases(IdentifierCollection $collection){
        $this->identifierAliasList[$collection->sourcePathInfo->path][] = $collection;
    }
}