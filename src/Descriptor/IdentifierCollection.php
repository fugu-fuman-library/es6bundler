<?php
namespace Fuman\Es6Builder\Descriptor;

use Fuman\Es6Builder\PackageInfo;
use Fuman\Es6Builder\PathInfo;

/**
 * A collection of identifiers and their possible alias
 */
class IdentifierCollection {
    const DEFAULT_KEY = 'defaultExport';
    const WILDCARD_KEY = 'contextExport';
    
    /**
     * Parses the source strings and creates a sanitized identifier collection from them.
     */
    public static function createInstance(string $identifierSource, string $defaultSource = '', string $nameSpaceSource = ''): IdentifierCollection {
        $identifierCollection = new static();
        $identifierCollection->identifiers = [];
        
        if(!empty($defaultSource)){
            $identifierCollection->hasDefault = true;
            static::parseIdentifierSource($identifierCollection, $defaultSource, true);
        }

        if(!empty($nameSpaceSource)){
            static::parseIdentifierSource($identifierCollection, $nameSpaceSource);
        }
        
        if(!empty($identifierSource)){
            foreach(explode(',', $identifierSource) as $s){
                static::parseIdentifierSource($identifierCollection, $s);
            }
        }
        
        return $identifierCollection;
    }

    /**
     * Parses a part of a source string, tries to extract information about the identifier and his alias
     * This Method normalizes the default key word to defaultExport and the wildcard (*) keyword to contextExport
     * 
     * @param IdentifierCollection $identifierCollection
     * @param string $source
     * @param bool $isDefault Is default identifier source from import
     */
    public static function parseIdentifierSource(IdentifierCollection $identifierCollection, string $source, bool $isDefault = false): void {
        $parts = array_map('trim', explode(' as ', str_replace(["\n","\t"], ' ', $source)));
        
        switch($parts[0]){
            case '*':
                $parts[0] = static::WILDCARD_KEY;
                break;
            case 'default':
                $parts[0] = static::DEFAULT_KEY;
                break;
        }
        
        $count = count($parts);
        if($isDefault && $count === 1){
            array_unshift($parts, static::DEFAULT_KEY);
            $count++;
        }
        
        $identifierCollection->identifiers[$parts[0]] = $count>1 ? $parts[1] : $parts[0];
    }
    
    public bool $hasDefault = false;
    
    public array $identifiers;
    
    public PathInfo $importPathInfo;
    
    public PathInfo $sourcePathInfo;
    
    public function injectFunctionParametersAndArguments(array &$functionParameters, array &$functionArguments){
        $prefix = $this->importPathInfo->identifierPrefix;
        
        foreach($this->identifiers as $identifier=>$alias){
            $functionParameters[] = $alias;
            $functionArguments[] = $prefix . $identifier;
        }
    }

    /**
     * Remove unnecessary proxy/index files/hops (export from import)
     * We do not split the imports along different paths!
     *
     * @param PackageInfo $packageInfo
     *
     * @return IdentifierCollection[]
     */
    public function updateAliasedIdentifiers(PackageInfo $packageInfo){
        $identifierCollections = [];
        
        if(isset($packageInfo->identifierAliasList[$this->importPathInfo->path])){
            foreach($packageInfo->identifierAliasList[$this->importPathInfo->path] as $identifierCollection){
      
                /**
                 * file a {$this}
                 * import {bbbb as aaaa} from b
                 * 
                 * file b {$identifierCollection}
                 * export {cccc as bbbb} from c
                 * 
                 * -> {$newIdentifierCollection}
                 * import {cccc as aaaa} from c
                 */
                $sameIdentifiers = [];
                foreach($identifierCollection->identifiers as $identifier=>$alias){
                    if(isset($this->identifiers[$alias])){
                        $sameIdentifiers[$identifier] = $this->identifiers[$alias];
                    }
                }
                
                if(!empty($sameIdentifiers)){
                    $newIdentifierCollection = new IdentifierCollection();
                    $newIdentifierCollection->sourcePathInfo = $this->sourcePathInfo;
                    $newIdentifierCollection->importPathInfo = $identifierCollection->importPathInfo;
                    $newIdentifierCollection->identifiers = $sameIdentifiers;
                    $identifierCollections[] = $newIdentifierCollection;
                }
            }
        }
        
        return $identifierCollections;
    }
}