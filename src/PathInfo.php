<?php
namespace Fuman\Es6Builder;


class PathInfo {
    public static function getInstanceFromPath($path, Config $config): PathInfo{
        foreach($config->bundles as $bundle){
            $pos = strpos($path, $bundle);

            if($pos !== false){
                if(str_ends_with($bundle, '/')){
                    $bundle = substr($bundle, 0, -1);
                }
                return new PathInfo($bundle, $path, $config, substr($path, $pos + strlen($bundle)));
            }
        }

        return new PathInfo($path, $path, $config);
    }
    
    public string $key;

    public string $path;

    public string $identifierPrefix;

    public string $packagePath;
    
    public bool $isPackage;
    
    public Config $config;
    
    public function __construct(string $key, string $path, Config $config, string $identifierPrefix = '') {
        $this->key = $key;
        $this->path = $path;
        $this->config = $config;
        $this->identifierPrefix = str_replace(['/', '.', '-'], '', substr($identifierPrefix, 0, strrpos($identifierPrefix, '.')));
        $this->isPackage = !empty($identifierPrefix);
        $this->packagePath =  $this->isPackage ? substr($path, 0, strlen($path) - strlen($identifierPrefix)) . '/index.js' : $path;
    }
}