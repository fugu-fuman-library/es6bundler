<?php
namespace Fuman\Es6Builder;

interface Parser {
    public function parse(SourceInfo $sourceInfo): void;
}
