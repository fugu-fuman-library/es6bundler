<?php
namespace Fuman\Es6Builder;


class Util {
    /**
     * Create from two absolute paths a relative path, 
     * where the $relativePath is the relative path from the $mainPath to the $otherPath
     * @param string $mainPath
     * @param string $otherPath
     *
     * @return string
     */
    public static function createRelativePath(string $mainPath, string $otherPath): string {
        $mainParts = explode('/', $mainPath);
        $otherParts = explode('/', $otherPath);
        $i = 0;
        
        foreach($mainParts as $i=>$part){
            if($part !== $otherParts[$i])break;
        }
        
        $relativePath = str_repeat('../', count($mainParts) - ($i+1)) . implode('/', array_slice($otherParts, $i));

        if($relativePath[0] !== '.'){
            $relativePath = './' . $relativePath;
        }
        
        return $relativePath;
    }
    
    public static function normalizePath(string $path):string {
        return str_replace('\\', '/', $path);
    }
}