<?php

namespace Fuman\Es6Builder;

use Fuman\Es6Builder\Statement\Export;
use Fuman\Es6Builder\Statement\Import;

/**
 * Not functional parser, experimenting parsing the source without regex
 * More about concepts how one can do it
 */
class SimpleParser implements Parser {
    protected int $_index;

    protected int $_lineNumberIndex;

    protected int $_len;

    protected string $_source;

    public function parse(SourceInfo $sourceInfo): void {
        $source = $sourceInfo->source;
        $this->_index = -1;
        $this->_len = strlen($source);
        $this->_source = $source;
        $this->_lineNumberIndex = 0;

        while(++$this->_index < $this->_len) {
            if($this->_source[$this->_index] === "\n") {
                $this->_lineNumberIndex++;
            }

            if(!$this->isWhiteSpace(-1) && $this->_index !== 0) {
                continue;
            }

            switch($this->getSubstring(6)) {
                case 'import':
                    if(!$this->wasExpressionEnd()){
                        continue 2;
                    }
                    
                    $import = new Import($this->_index);
                    $this->_index += 6;

                    if(!$this->isWhiteSpace()) {
                        continue 2;
                    }

                    if($this->searchIdentifier('from')){
                        $import->identifierSource = substr($this->_source, $this->_index, $fromPos - $this->_index);
                        $this->_index = $fromPos;
                    }
                    $fromPos = stripos($this->_source, ' from ', $this->_index);
                    if($fromPos !== false){

                    }
                    
                    $this->search('"\'');
                    $pathStart = ++$this->_index;
                    $pathEnd = strpos($this->_source, $this->_source[$this->_index - 1], $this->_index);
                    if($pathEnd === false){
                        throw new Exception('syntax error');
                    }
                    
                    $import->pathSource = substr($this->_source, $pathStart, $pathEnd - $this->_index);

                    //search line break or ;
                    $this->_index = $pathEnd;
                    $this->consumeWhiteSpace();
                    if($this->_source[$this->_index] === ';') {
                        $this->_index++;
                    }

                    //Try to detect line ending, if any found, add to import config, but the index should stay before!
                    $lineEnding = strpos(substr($this->_source, $this->_index, 5), "\n");
                    if($lineEnding !== false) {
                        //index is until before line ending
                        $this->_index += $lineEnding;
                        //end pos should be after line ending -> therefore it is removed when this statement is replaced.
                        $import->endIndex = $this->_index + 1;
                    }
                    else {
                        $import->endIndex = $this->_index;
                    }

                    $sourceInfo->imports[] = $import;
                    break;
                case 'export':
                    if(!$this->wasExpressionEnd()){
                        continue 2;
                    }
                    
                    $export = new Export($this->_lineNumberIndex, $this->_index);
                    $this->_index += 6;

                    if(!$this->isWhiteSpace()) {
                        continue 2;
                    }

                    $innerStartIndexObject = strpos(substr($this->_source, $this->_index, 5), "{");
                    if($innerStartIndexObject !== false){
                        $startIndexObject = $this->_index + $innerStartIndexObject + 1;
                        $this->_index = strpos($this->_source, '}', $startIndexObject);
                        $export->exportSource = substr($this->_source, $startIndexObject, $this->_index - $startIndexObject);
                        $export->endIndex = $this->_index+1;
                    }
                    else {
                        $this->advanceToAlpha();
                        if($this->getSubstring(7) === 'default') {
                            $this->_index += 7;
                            $export->isDefault = true;
                            $export->endIndex = $this->_index;
                        }
                        else {
                            $typeStart = $this->_index;
                            $this->advanceToWhiteSpace();

                            $export->type = substr($this->_source, $typeStart, $this->_index - $typeStart);
                            $export->endIndex = $typeStart;

                            $this->advanceToAlpha();
                            $identifierStart = $this->_index;
                            $this->consumeIdentifier();
                            $export->identifierSource = trim(substr($this->_source, $identifierStart, $this->_index - $identifierStart));
                        }
                    }

                    $sourceInfo->exports[] = $export;
                    break;
            }
        }
    }

    public function advanceToAlpha() {
        while(++$this->_index < $this->_len) {
            if(ctype_alpha($this->_source[$this->_index])) return;
        }
    }

    public function consumeIdentifier() {
        while(++$this->_index < $this->_len) {
            if(!(ctype_alpha($this->_source[$this->_index]) || $this->_source[$this->_index] === '_')) return;
        }
    }

    public function advanceToWhiteSpace() {
        while(++$this->_index < $this->_len) {
            if(trim($this->_source[$this->_index]) === '') return;
        }
    }

    public function consumeWhiteSpace() {
        while(++$this->_index < $this->_len) {
            if(trim($this->_source[$this->_index]) !== '') return;
        }
    }

    public function searchI(string $string) {
        while(++$this->_index < $this->_len) {
            if(stripos($string, $this->_source[$this->_index]) !== false) return;
        }
    }

    public function search(string $string) {
        while(++$this->_index < $this->_len) {
            if(strpos($string, $this->_source[$this->_index]) !== false) return;
        }
    }
    
    public function searchIdentifier(string $value){
        $firstChar = $value[0];
        $len = strlen($value);
        
        while($this->_index < $this->_len) {
            $pos = strpos($firstChar, $this->_source, $this->_index);
            if($pos === false){
                $this->_index = $this->_len;
                return false;
            }
            
            $this->_index = $pos;
            if($this->isWhiteSpace(-1) && $this->getSubstring($len) === $value){
                $this->_index += $len;
                if($this->isWhiteSpace(1)){
                    return true;    
                }
            }
            
            if($this->wasExpressionEnd(1)){
                return false;
            }
            
            $this->_index++;
        }
        
        return false;
    }

    public function getSubstring(int $l): string {
        return strtolower(substr($this->_source, $this->_index, $l));
    }

    public function isWhiteSpace(int $i = 0) {
        return trim($this->_source[$this->_index + $i]) === '';
    }
    
    public function wasExpressionEnd($l = 3){
        if($this->_index === 0)return true;
        
        while($l--){
            if($this->_source[$this->_index - $l] === ';' || $this->_source[$this->_index - $l] === "\n")return true;
        }
        
        return false;
    }
}