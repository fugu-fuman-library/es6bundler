<?php
namespace Fuman\Es6Builder\Statement;

use Fuman\Es6Builder\Descriptor\IdentifierCollection;
use Fuman\Es6Builder\PathInfo;

class Export {
    public int $startIndex;
    
    public int $endIndex;
    
    public bool $isDefault;
    
    public string $type;
    
    public string $identifierSource;
    
    public string $exportSource;

    public string $pathSource;

    /**
     * Import constructor.
     *
     * @param int $startIndex
     */
    public function __construct(int $startIndex) {
        $this->startIndex = $startIndex;
    }

    /**
     * @param $identifierPrefix
     *
     * @return string
     */
    public function createReplacement($identifierPrefix): string {
        if($this->isDefault){
            if(empty($this->identifierSource)){
                //We can append exportSource directly, as the exportSource for a default export must be a object or empty.
                return 'const defaultExport =' . (empty($this->exportSource) ? ' ' : "{{$this->exportSource}}");
            }
            else {
                return '';   
            }
        }
        
        return '';
    }
    
    public function createExportStatementPart(PathInfo $sourcePathInfo): string {
        $identifierPrefix = $sourcePathInfo->identifierPrefix;
        
        if($this->isDefault){
            return $identifierPrefix . 'defaultExport';
        }

        if(!empty($this->identifierSource)){
            return $identifierPrefix . $this->identifierSource;
        }
        else if(!empty($this->exportSource)){
            $identifierCollection = $this->createIdentifierCollection($sourcePathInfo);
            
            $exportStatements = [];
            foreach($identifierCollection->identifiers as $alias){
                $exportStatements[] = $identifierPrefix . $alias;
            }
            
            return implode(', ', $exportStatements);
        }
        
        return '';
    }
    
    public function createReturnStatementPart(): string {
        if($this->isDefault){
            if(empty($this->identifierSource)){
                return 'defaultExport';
            }
            else {
                return $this->identifierSource;
            }
        }

        if(!empty($this->identifierSource)){
            return $this->identifierSource;
        }
        else if(!empty($this->exportSource)){
            return $this->exportSource;
//            return str_replace('default as ', '', $this->exportSource);
        }

        return '';
    }
    
    public function createIdentifierCollection(PathInfo $sourcePathInfo){
        $identifierCollection = IdentifierCollection::createInstance($this->exportSource);
        $identifierCollection->importPathInfo = PathInfo::getInstanceFromPath(realpath(dirname($sourcePathInfo->path) . '/' . trim($this->pathSource)), $sourcePathInfo->config);
        $identifierCollection->sourcePathInfo = $sourcePathInfo;
        return $identifierCollection;
    }
}