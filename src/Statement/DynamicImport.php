<?php
namespace Fuman\Es6Builder\Statement;

use Fuman\Es6Builder\PathInfo;

class DynamicImport {
    public int $startIndex;

    public int $endIndex;
    
    /**
     * Import constructor.
     *
     * @param int $startIndex
     */
    public function __construct(int $startIndex) {
        $this->startIndex = $startIndex;
    }
    
    public function createReplacement(PathInfo $pathInfo): string {
        $webPath = substr($pathInfo->path, strlen($pathInfo->config->rootPath));
        return $pathInfo->config->dynamicModuleImportFunctionName . "('$webPath',";
    }
}