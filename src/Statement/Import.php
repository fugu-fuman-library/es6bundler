<?php
namespace Fuman\Es6Builder\Statement;

use Fuman\Es6Builder\Descriptor\IdentifierCollection;
use Fuman\Es6Builder\PathInfo;

class Import {
    public int $startIndex;
    
    public int $endIndex;
    
    public string $defaultSource;
    
    public string $nameSpaceSource;
    
    public string $identifierSource;
    
    public string $pathSource;
    
    public bool $samePackage;
    
    public PathInfo $pathInfo;
    
    public IdentifierCollection $identifierCollection;
    
    /**
     * Import constructor.
     * @param int $startIndex
     */
    public function __construct(int $startIndex) {
        $this->startIndex = $startIndex;
    }
    
    public function init(PathInfo $sourcePathInfo){
        $this->pathInfo = str_contains($this->pathSource, '/') || str_contains($this->pathSource, '.') 
            ? PathInfo::getInstanceFromPath(realpath(dirname($sourcePathInfo->path) . '/' . $this->pathSource), $sourcePathInfo->config)
            : PathInfo::getInstanceFromPath($this->pathSource, $sourcePathInfo->config);
        
        
        //if key is same, we do not have to parse, because we throw it away.
        $this->samePackage = $this->pathInfo->key === $sourcePathInfo->key;

        $this->identifierCollection = IdentifierCollection::createInstance($this->identifierSource, $this->defaultSource, $this->nameSpaceSource);
        $this->identifierCollection->importPathInfo = $this->pathInfo;
        $this->identifierCollection->sourcePathInfo = $sourcePathInfo;
    }
}