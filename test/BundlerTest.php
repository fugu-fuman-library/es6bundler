<?php

namespace Fuman\Es6Builder\Test;

use Fuman\Es6Builder\Bundler;
use Fuman\Es6Builder\Config;
use JetBrains\PhpStorm\Pure;
use PHPUnit\Framework\TestCase;

class BundlerTest extends TestCase {
    #[Pure]
    public static function createConfig(): Config{
        return new Config(__DIR__ . '/resource', ['bundle1']);
    }
    
    public function testTransformBasicFile() {
        $bundler = new Bundler(BundlerTest::createConfig());
        $content = $bundler->transformFile(__DIR__  . '/resource/import.js');
        $this->assertEquals(file_get_contents(__DIR__ . '/resource/import.js'), $content);
    }
    
    public function testTransformPackageFile(){
        $bundler = new Bundler(BundlerTest::createConfig());
        $content = $bundler->transformFile(__DIR__  . '/resource/bundle1/a.js');
        $this->assertEquals(file_get_contents(__DIR__ . '/resource/bundle1_bundle.js'), $content);
    }
}
