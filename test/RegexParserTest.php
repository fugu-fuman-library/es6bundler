<?php

namespace Fuman\Es6Builder\Test;

use Fuman\Es6Builder\PathInfo;
use Fuman\Es6Builder\RegexParser;
use Fuman\Es6Builder\SourceInfo;
use Fuman\Es6Builder\Statement\Export;
use Fuman\Es6Builder\Statement\Import;
use PHPUnit\Framework\TestCase;

class RegexParserTest extends TestCase {
    protected function createPathInfo($path): PathInfo {
        return new PathInfo($path, __DIR__ . '/resource/' . $path, BundlerTest::createConfig());
    }
    
    public function testEmptyFile(){
        $parser = new RegexParser();
        
        $sourceInfo = new SourceInfo($this->createPathInfo('empty.js'));
        $parser->parse($sourceInfo);
        
        $this->assertEquals(0, count($sourceInfo->imports));
        $this->assertEquals(0, count($sourceInfo->dynamicImports));
        $this->assertEquals(0, count($sourceInfo->exports));
    }
    
    public function testExport(){
        $parser = new RegexParser();
        
        $sourceInfo = new SourceInfo($this->createPathInfo('export.js'));
        $parser->parse($sourceInfo);

        $this->assertEquals(0, count($sourceInfo->imports));
        $this->assertEquals(0, count($sourceInfo->dynamicImports));
        $this->assertEquals(4, count($sourceInfo->exports));
    }

    public function testImport(){
        $parser = new RegexParser();
        
        $sourceInfo = new SourceInfo($this->createPathInfo('import.js'));
        $parser->parse($sourceInfo);
        
        $this->assertEquals(2, count($sourceInfo->imports));
        $this->assertEquals(0, count($sourceInfo->dynamicImports));
        $this->assertEquals(0, count($sourceInfo->exports));

        $imports = array_values($sourceInfo->imports);
        
        $this->assertEquals('testConst, testFunction as testAliasFunction', $imports[0]->identifierSource);
        $this->assertEquals(['testConst'=>'testConst', 'testFunction' => 'testAliasFunction'], $imports[0]->identifierCollection->identifiers);
        $this->assertEquals(['contextExport'=>'all'], $imports[1]->identifierCollection->identifiers);
    }
    
    public function testDynamicImport(){
        $parser = new RegexParser();

        $sourceInfo = new SourceInfo($this->createPathInfo('dynamic-import.js'));
        $parser->parse($sourceInfo);

        $this->assertEquals(0, count($sourceInfo->imports));
        $this->assertEquals(1, count($sourceInfo->dynamicImports));
        $this->assertEquals(0, count($sourceInfo->exports));
        
        $this->assertEquals($sourceInfo->pathInfo->config->dynamicModuleImportFunctionName . "('/dynamic-import.js',", $sourceInfo->dynamicImports[0]->createReplacement($sourceInfo->pathInfo));
    }
    
    public function testMDNImport(){
        $parser = new RegexParser();

        $sourceInfo = new SourceInfo($this->createPathInfo('mdn-import.js'));
        $parser->parse($sourceInfo);


        $this->assertEquals(11, count($sourceInfo->imports));
        $this->assertEquals(0, count($sourceInfo->dynamicImports));
        $this->assertEquals(0, count($sourceInfo->exports));

        $imports = array_values($sourceInfo->imports);
        $this->validateImport($imports[0], []);
        $this->validateImport($imports[1], ['defaultExport' =>'name_1'], true);
        $this->validateImport($imports[2], ['contextExport' =>'name_2']);
        $this->validateImport($imports[3], ['member_3' =>'member_3']);
        $this->validateImport($imports[4], ['member' =>'alias_4']);
        $this->validateImport($imports[5], ['member1_5' =>'member1_5', 'member2_5' =>'member2_5']);
        $this->validateImport($imports[6], ['member1_6' =>'member1_6', 'member2' =>'alias2_6']);
        $this->validateImport($imports[7], ['defaultExport' =>'defaultMember_7', 'member_7' =>'member_7'], true);
        $this->validateImport($imports[8], ['defaultExport' =>'defaultMember_8', 'contextExport' =>'alias_8'], true);
        $this->validateImport($imports[9], ['defaultExport' =>'defaultMember_9'], true);
        
        #@todo is this a bug, as this is not marked as a default identifier list
        $this->validateImport($imports[10], ['defaultExport' =>'defaultMember_10']);
    }
    
    public function testMDNExport(){
        $parser = new RegexParser();

        $sourceInfo = new SourceInfo($this->createPathInfo('mdn-export.js'));
        $parser->parse($sourceInfo);


        $this->assertEquals(0, count($sourceInfo->imports));
        $this->assertEquals(0, count($sourceInfo->dynamicImports));
        $this->assertEquals(10, count($sourceInfo->exports));
        
        $exports = $sourceInfo->exports;
        $this->validateExport($exports[0], $sourceInfo->pathInfo, ['name1'=>'name1', 'name2'=>'name2', 'nameN'=>'nameN']);
        $this->validateExport($exports[1], $sourceInfo->pathInfo, ['variable1'=>'name1_1', 'variable2'=>'name2_1', 'nameN_1'=>'nameN_1']);
        $this->validateExport($exports[2], $sourceInfo->pathInfo, 'name1_2');
        $this->validateExport($exports[3], $sourceInfo->pathInfo, 'name1_3');
        $this->validateExport($exports[4], $sourceInfo->pathInfo, '', true);
        $this->validateExport($exports[5], $sourceInfo->pathInfo, 'name1_5', true);
        $this->validateExport($exports[6], $sourceInfo->pathInfo, ['name1'=>'default']);
        $this->validateExport($exports[7], $sourceInfo->pathInfo, '*');
        $this->validateExport($exports[8], $sourceInfo->pathInfo, ['name1_8'=>'name1_8', 'name2_8'=>'name2_8', 'nameN_8' => 'nameN_8']);
        $this->validateExport($exports[9], $sourceInfo->pathInfo, ['import1'=>'name1_9', 'import2'=>'name2_9', 'nameN_9' => 'nameN_9']);
    }
    
    private function validateImport(Import $import, $identifiers, $hasDefault = false){
        $info = var_export($import, true);
        $this->assertEquals($identifiers, $import->identifierCollection->identifiers, 'The identifiers do not match ' . $info);
        $this->assertEquals(false, $import->samePackage, 'It should not be same package ' . $info);
        $this->assertEquals($hasDefault, $import->identifierCollection->hasDefault, 'About has default identifier ' . $info);
        if(!$hasDefault){
            $this->assertEquals('', $import->defaultSource);
        }
    }
    
    private function validateExport(Export $export, PathInfo $pathInfo, $identifiers, bool $isDefault = false){
        $info = var_export($export, true);

        if(isset($export->exportSource)){
            $identifierCollection = $export->createIdentifierCollection($pathInfo);
            $this->assertEquals($identifiers, $identifierCollection->identifiers, 'The identifiers do not match ' . $info);
            $this->assertEquals(false, $identifierCollection->hasDefault, 'About has default identifier ' . $info);
        }
        else {
            $this->assertEquals($identifiers, $export->identifierSource ?? '');
        }
        
        $this->assertEquals($isDefault, $export->isDefault, $info);
    }
}
