<?php
namespace Fuman\Es6Builder\Test;

use Fuman\Es6Builder\RegexParser;
use PHPUnit\Framework\TestCase;

class RegexTest extends TestCase {
    public function testImport(){
        $text = 'import defaultMember_8, * as alias_8 from "module-name8";';
        $regex = '/(import)\s+(?:(\w*?)(?:\s*,\s*)?(\*\s+as\s+\w+)?(?:{([^}]+)})?\s+from\s+)?([\'"])(.+?)\5([^\n;]*;?)/';

        $this->assertTrue(str_contains(RegexParser::REGEX, trim($regex,'/')), 'The test regex does not exists in the regex of the parser');
        
        preg_match_all($regex, $text, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
        
        $match = $matches[0];
        $this->assertEquals($text, $match[0][0]);
        $this->assertEquals('import', $match[1][0]);
        $this->assertEquals('defaultMember_8', $match[2][0]);
        $this->assertEquals('* as alias_8', $match[3][0]);
        $this->assertEquals('module-name8', $match[6][0]);
    }
    
    public function testExport(){
        $text = 'export let x = 1, y = 2, z;';
        $regex = '/(export)\s+(default\s+)?(?|(?|({)([^};]+)}|(\*))((\s+from\s+)([\'"])(.+?)\7)?|((?:async\s+)?\w+\s+)?([\w_]+))([^\n;]*;?)/';
        
        $this->assertTrue(str_contains(RegexParser::REGEX, trim($regex,'/')), 'The test regex does not exists in the regex of the parser');

        preg_match_all($regex, $text, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);

        $match = $matches[0];
        $this->assertEquals($text, $match[0][0]);
        $this->assertEquals('export', $match[1][0]);
        $this->assertEquals('', $match[2][0]);
        $this->assertEquals('let ', $match[3][0]);
        $this->assertEquals('x', $match[4][0]);
    }
}