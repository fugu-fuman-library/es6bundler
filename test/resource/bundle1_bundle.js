const [aaConst, aaFunction, adefaultExport, aClassA] = (function(){

const aConst = 0;

function aFunction(){

}

const defaultExport = class extends aFunction {

}

class ClassA {

}

return [aConst,aFunction,defaultExport,ClassA];

})();
export {aaConst, aaFunction, adefaultExport, aClassA};
const [bbConst, bbFunction, bdefaultExport, bClassB] = (function(){

const bConst = 0;

function bFunction(){

}

const defaultExport = class {

}

class ClassB {

}

return [bConst,bFunction,defaultExport,ClassB];

})();
export {bbConst, bbFunction, bdefaultExport, bClassB};
