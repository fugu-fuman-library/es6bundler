/**
 * files with imports only
 */
import {testConst, testFunction as testAliasFunction} from './export.js';

import * as all from './empty.js';

testAliasFunction(testConst);