/**
 * adapted from:
 * https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/export
 */

let name1, name2, nameN;

export { name1, name2, nameN };
export { variable1 as name1_1, variable2 as name2_1, nameN_1};
export let name1_2, name2_2,nameN_2; // oder: var
export let name1_3 = 'name1', name2_3 = 'name2', nameN_3; // oder: var, const

// export default expression;
export default function () {} // oder: class, function*
export default function name1_5 () {} // oder: class, function*
export { name1 as default};

export * from 'resource1';
export { name1_8, name2_8, nameN_8 } from 'resource2';
export { import1 as name1_9, import2 as name2_9, nameN_9 } from 'resource3';