/**
 * adapted from:
 * https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/import
 */

import "module-name";
import name_1 from "module-name1";
import * as name_2 from "module-name2";
import { member_3 } from "module-name3";
import { member as alias_4 } from "module-name4";
import { member1_5 , member2_5 } from "module-name5";
import { member1_6 , member2 as alias2_6 } from "module-name6";
import defaultMember_7, {member_7} from "module-name7";
import defaultMember_8, * as alias_8 from "module-name8";
import defaultMember_9 from "module-name9";
import {default as defaultMember_10} from "module-name10";

