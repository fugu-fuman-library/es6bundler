# ES6 bundler
A ES6 bundler written in PHP.

This bundler tries to change only the import and export statements and ignore the rest.
As we do not have to parse the source code completely, it should be possible to create a performant script.

## Unsupported exports
Until now, comma separated exports (shorthand version) is not supported.
As we need there a nice way to parse javascript including all possible expressions.

## Todo
Detecting comma-separated exports and throwing an informative exception about this is not supported.

## Usage

```php
$bundler = new \Fuman\Es6Builder\Bundler(new \Fuman\Es6Builder\Config('path/to/web/root/', ['bundle1', 'library/bundle2']));
$transformedAndPackedSource = $bundler->transformFile('path/to/file.js');
```



